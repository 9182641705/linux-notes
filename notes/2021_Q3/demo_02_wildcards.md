# Linux HFS
## Wildcards

## List of Wildcards

|Wildcard|Description|
|------|-------|
|`*` | zero or more ascii characters|
|`?` | any one ascii character|
|`[abc]` | a, b or c: character set |
|`[!abc]`| any other ascii char than listed |
|`[^abc]`| same as `[!abc]`|
|`[a-z]` | any one ascii chars between a-z (range by ascii value)|
|`[!a-z]`| inverse of `[a-z]`|
|`[^a-z]`| same as `[!a-z]`|

## Brace Expansion
|Wildcard|Description|
|------|-------|
|`{1..10}` | expand 1 to 10, increment by 1 |
|`{1..10..2}` | expand 1 to 10, increment value is 2 (BASH 4.x or later|
|`{01..10}`| same as first one, with leading zeroes (BASH 4.x or later|
|`{a..z}` | lowercase alphabets|
|`{a..z..2}`|lowercase alphabets, jump 2 chars|
|`{alpha,beta,gamma}`| 3 words |


## Examples

**BASH V3.x**

```
$ echo $BASH_VERSION
3.2.57(1)-release

$ echo {01..10}
1 2 3 4 5 6 7 8 9 10

$ echo {01..10..2}
{01..10..2}

$ echo {01-10}
{01-10}

$ echo {a..z}
a b c d e f g h i j k l m n o p q r s t u v w x y z

$ echo {sample,demo,test}.{txt,csv,dat}
sample.txt sample.csv sample.dat demo.txt demo.csv demo.dat test.txt test.csv test.dat
```

**BASH V4.x+*

```
$ echo $BASH_VERSION 
4.4.23(1)-release

$ echo {01..10}
01 02 03 04 05 06 07 08 09 10

$ echo {01..1000..150}
0001 0151 0301 0451 0601 0751 0901

$ echo {01-10}
{01-10}

$ echo {a..z}
a b c d e f g h i j k l m n o p q r s t u v w x y z

$ echo {a..z..3}
a d g j m p s v y
$ echo {sample,demo,test}.{txt,csv,dat}
sample.txt sample.csv sample.dat demo.txt demo.csv demo.dat test.txt test.csv test.dat

$ echo {10..01}
10 09 08 07 06 05 04 03 02 01

# sample files
$ mkdir temp
$ cd temp
$ ls -a
.  ..
$ touch test_{s..z}{1..15..3}.{css,js,c}
$ touch test_{a..g}{01..15..3}.{txt,csv,html}
$ ls
...
...  
test_c04.html  test_e13.csv   test_s13.js    test_u7.css   test_x1.c     test_z7.js
test_c04.txt   test_e13.html  test_s1.c      test_u7.js    test_x1.css
test_c07.csv   test_e13.txt   test_s1.css    test_v10.c    test_x1.js
test_c07.html  test_f01.csv   test_s1.js     test_v10.css  test_x4.c
```

**Wildcard Demo on the files created**

```
$ ls *.c
test_s10.c  test_t10.c  test_u10.c  test_v10.c  test_w10.c  test_x10.c  test_y10.c  test_z10.c
test_s13.c  test_t13.c  test_u13.c  test_v13.c  test_w13.c  test_x13.c  test_y13.c  test_z13.c
test_s1.c   test_t1.c   test_u1.c   test_v1.c   test_w1.c   test_x1.c   test_y1.c   test_z1.c
test_s4.c   test_t4.c   test_u4.c   test_v4.c   test_w4.c   test_x4.c   test_y4.c   test_z4.c
test_s7.c   test_t7.c   test_u7.c   test_v7.c   test_w7.c   test_x7.c   test_y7.c   test_z7.c

$ ls test_??.c
test_s1.c  test_t1.c  test_u1.c  test_v1.c  test_w1.c  test_x1.c  test_y1.c  test_z1.c
test_s4.c  test_t4.c  test_u4.c  test_v4.c  test_w4.c  test_x4.c  test_y4.c  test_z4.c
test_s7.c  test_t7.c  test_u7.c  test_v7.c  test_w7.c  test_x7.c  test_y7.c  test_z7.c

$ ls test_[aeiou]?.c
test_u1.c  test_u4.c  test_u7.c

$ ls test_[aeiou][24680].c
test_u4.c

$ ls test_[aeiou][!24680].c
test_u1.c  test_u7.c

$ ls test_[aeiou][^24680].c
test_u1.c  test_u7.c

$ ls test_[aeiou][^24680].??
test_u1.js  test_u7.js

$ ls test_[aeiou][^24680].*
test_u1.c  test_u1.css  test_u1.js  test_u7.c  test_u7.css  test_u7.js

$ ls test_[aeiou][^24680].c*
test_u1.c  test_u1.css  test_u7.c  test_u7.css

$ ls test_[aeiou][^24680].??
test_u1.js  test_u7.js
```